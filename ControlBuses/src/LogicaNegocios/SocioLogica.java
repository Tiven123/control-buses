/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LogicaNegocios;

import Entidades.Socio;
import BaseDatos.SocioBD;

/**
 *
 * @author Steven
 */
public class SocioLogica {

    SocioBD socioBD = new SocioBD();

    public boolean insertar(Socio nuevo) {
        if (nuevo.getNombre() == null || nuevo.getApellidos() == null || nuevo.getLinea() == null
                || nuevo.getNumeroBus() == 0 || nuevo.getCapacidad() == 0 || nuevo.getAyudante() == null
                || nuevo.getChofer() == null || nuevo.getEstado() == false) {
            System.out.println("Error: Datos Incompletos");
            return false;
        } else {
            try {
                if (socioBD.insertar(nuevo)) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception ex) {
                System.out.println(ex);
                return false;
            }
        }
    }

}
