/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseDatos;
import Entidades.Recorrido;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
/**
 *
 * @author Steven
 */
public class RecorridoBD {

    public RecorridoBD() {
    }
    
    
    public boolean insertar(Recorrido nuevo) throws Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "insert into recorrido(numerobus, descripcion, duracion, zona, cantidad, estado)"
                    + " values(?,?,?,?,?,?)";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setInt(1, nuevo.getNumeroBus());
            smt.setString(2, nuevo.getDescripcion());
            smt.setTime(3, nuevo.getDuracion());
            smt.setString(4, nuevo.getZona());
            smt.setInt(5, nuevo.getCantidad());
            smt.setBoolean(6, nuevo.getEstado());
            return smt.executeUpdate() > 0;
        } catch (SQLException ex) {
            System.out.println("Error en Base de datos: " + ex);
            return false;
        }
    }

    public Recorrido buscar(int codigo) {
        Recorrido resultado = new Recorrido();
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from recorrido where codigo = ?";
            PreparedStatement smt = con.prepareStatement(sql);
            //Parametros
            smt.setInt(1, codigo);
            ResultSet resultadoBusqueda = smt.executeQuery();
            if (resultadoBusqueda.next()) {
                resultado.setCodigo(resultadoBusqueda.getInt("codigo"));
                resultado.setNumeroBus(resultadoBusqueda.getInt("numerobus"));
                resultado.setDescripcion(resultadoBusqueda.getString("descripcion"));
                resultado.setDuracion(resultadoBusqueda.getTime("duracion"));
                resultado.setZona(resultadoBusqueda.getString("zona"));
                resultado.setCantidad(resultadoBusqueda.getInt("cantidad"));
                resultado.setEstado(resultadoBusqueda.getBoolean("estado"));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return resultado;
    }

    public LinkedList<Recorrido> reporte() {
        LinkedList<Recorrido> lista = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from recorrido";
            PreparedStatement smt = con.prepareStatement(sql);
            ResultSet resultadoBusqueda = smt.executeQuery();
            while (resultadoBusqueda.next()) {
                Recorrido resultado = new Recorrido();
                resultado.setCodigo(resultadoBusqueda.getInt("codigo"));
                resultado.setNumeroBus(resultadoBusqueda.getInt("numerobus"));
                resultado.setDescripcion(resultadoBusqueda.getString("descripcion"));
                resultado.setDuracion(resultadoBusqueda.getTime("duracion"));
                resultado.setZona(resultadoBusqueda.getString("zona"));
                resultado.setCantidad(resultadoBusqueda.getInt("cantidad"));
                resultado.setEstado(resultadoBusqueda.getBoolean("estado"));
                lista.add(resultado);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return lista;
    }

    public boolean modificar(Recorrido nuevo) throws SQLException, Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "update recorrido set numerobus = ?, descripcion = ?, "
                    + "duracion  = ?, zona = ?, cantidad = ?  where codigo = ? ";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setInt(1, nuevo.getNumeroBus());
            smt.setString(2, nuevo.getDescripcion());
            smt.setTime(3, nuevo.getDuracion());
            smt.setString(4, nuevo.getZona());
            smt.setInt(5, nuevo.getCantidad());
            smt.setInt(6, nuevo.getCodigo());
            return smt.executeUpdate() > 0;
        } catch (SQLException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public boolean eliminar(int codigo) throws SQLException, Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "update recorrido set estado = ? where codigo = ? ";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setBoolean(1, false);
            smt.setInt(2, codigo);
            return smt.executeUpdate() > 0;
        } catch (SQLException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }
    }

    
}
