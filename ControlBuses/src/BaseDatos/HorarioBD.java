/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseDatos;

import Entidades.Horario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 *
 * @author Steven
 */
public class HorarioBD {

    public HorarioBD() {
    }

    public boolean insertar(Horario nuevo) throws Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "insert horario(numerobus, descripcion, horasalida, horallegada, estado)"
                    + " values(?,?,?,?,?)";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setInt(1, nuevo.getNumeroBus());
            smt.setString(2, nuevo.getDescripcion());
            smt.setTime(3, nuevo.getHoraSalida());
            smt.setTime(4, nuevo.getHoraLlegada());
            smt.setBoolean(5, nuevo.getEstado());
            return smt.executeUpdate() > 0;
        } catch (SQLException ex) {
            throw ex;
        }
    }
    
    public Horario buscar(int codigo) {
        Horario resultado = new Horario();
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from horario where codigo = ?";
            PreparedStatement smt = con.prepareStatement(sql);
            //Parametros
            smt.setInt(1, codigo);
            ResultSet resultadoBusqueda = smt.executeQuery();
            if (resultadoBusqueda.next()) {
                resultado.setCodigo(resultadoBusqueda.getInt("codigo"));
                resultado.setNumeroBus(resultadoBusqueda.getInt("numerobus"));
                resultado.setDescripcion(resultadoBusqueda.getString("descripcion"));
                resultado.setHoraSalida(resultadoBusqueda.getTime("horasalida"));
                resultado.setHoraLlegada(resultadoBusqueda.getTime("horallegada"));
                resultado.setEstado(resultadoBusqueda.getBoolean("estado"));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return resultado;
    }
    
    public LinkedList<Horario> reporte() {
        LinkedList<Horario> lista = new LinkedList<>();
        try (Connection con = Conexion.conexion()) {
            String sql = "select * from horario";
            PreparedStatement smt = con.prepareStatement(sql);
            ResultSet resultadoBusqueda = smt.executeQuery();
            while (resultadoBusqueda.next()) {
                Horario resultado = new Horario();
                resultado.setCodigo(resultadoBusqueda.getInt("codigo"));
                resultado.setNumeroBus(resultadoBusqueda.getInt("numerobus"));
                resultado.setDescripcion(resultadoBusqueda.getString("descripcion"));
                resultado.setHoraSalida(resultadoBusqueda.getTime("horasalida"));
                resultado.setHoraLlegada(resultadoBusqueda.getTime("horallegada"));
                resultado.setEstado(resultadoBusqueda.getBoolean("estado"));
                lista.add(resultado);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return lista;
    }
    
    public boolean modificar(Horario nuevo) throws SQLException, Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "update horario set numerobus = ?, descripcion = ?, " +
                     "horasalida = ?, horallegada = ?, estado = ? where codigo = ? ";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setInt(1, nuevo.getNumeroBus());
            smt.setString(2, nuevo.getDescripcion());
            smt.setTime(3, nuevo.getHoraSalida());
            smt.setTime(4, nuevo.getHoraLlegada());
            smt.setBoolean(5, nuevo.getEstado());
            smt.setInt(6, nuevo.getCodigo());
           return smt.executeUpdate() > 0;
        } catch (SQLException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }
    }
    
    public boolean eliminar (int codigo) throws SQLException, Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "update horario set estado = ? where codigo = ? ";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setBoolean(1, true);
            smt.setInt(2,codigo);
           return smt.executeUpdate() > 0;
        } catch (SQLException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }
    }
    
}
