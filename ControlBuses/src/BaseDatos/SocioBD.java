/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseDatos;
import Entidades.Socio;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
/**
 *
 * @author Steven
 */
public class SocioBD {

    public SocioBD() {
    }
    
    public boolean insertar(Socio nuevo) throws Exception {
        try (Connection con = Conexion.conexion()) {
            String sql = "insert socio(nombre, apellidos, linea, numerobus, capacidad, ayudante, chofer, estado)"
                    + " values(?,?,?,?,?,?,?,?)";
            PreparedStatement smt = con.prepareStatement(sql);
            smt.setString(1, nuevo.getNombre());
            smt.setString(2, nuevo.getApellidos());
            smt.setString(3, nuevo.getLinea());
            smt.setInt(4, nuevo.getNumeroBus());
            smt.setInt(5, nuevo.getCapacidad());
            smt.setString(6, nuevo.getAyudante());
            smt.setString(7, nuevo.getChofer());
            smt.setBoolean(8, nuevo.getEstado());
            return smt.executeUpdate() > 0;
        } catch (SQLException ex) {
            System.out.println("Error en Base de datos: " + ex);
            return false;
        }
    }
    
}
