/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author Steven
 */
public class Socio {

    private int codigo;
    private String nombre;
    private String apellidos;
    private String linea;
    private int numeroBus;
    private int capacidad;
    private String ayudante;
    private String chofer;
    private String estado;

    public Socio() {
    }

    public Socio(int codigo, String nombre, String apellidos, String linea, int numeroBus, int capacidad, String ayudante, String chofer, String estado) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.linea = linea;
        this.numeroBus = numeroBus;
        this.capacidad = capacidad;
        this.ayudante = ayudante;
        this.chofer = chofer;
        this.estado = estado;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getLinea() {
        return linea;
    }

    public int getNumeroBus() {
        return numeroBus;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public String getAyudante() {
        return ayudante;
    }

    public String getChofer() {
        return chofer;
    }

    public String getEstado() {
        return estado;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public void setNumeroBus(int numeroBus) {
        this.numeroBus = numeroBus;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public void setAyudante(String ayudante) {
        this.ayudante = ayudante;
    }

    public void setChofer(String chofer) {
        this.chofer = chofer;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Socio{" + "codigo=" + codigo + ", nombre=" + nombre + ", apellidos=" + apellidos + ", linea=" + linea + ", numeroBus=" + numeroBus + ", capacidad=" + capacidad + ", ayudante=" + ayudante + ", chofer=" + chofer + ", estado=" + estado + '}';
    }
    
    

}
