/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.sql.Time;

/**
 *
 * @author Steven
 */
public class Horario {
    private int codigo;
    private int numeroBus;
    private String descripcion;
    private Time horaSalida;
    private Time horaLlegada;
    private boolean estado;

    public Horario() {
    }

    public Horario(int codigo, int numeroBus, String descripcion, Time horaSalida, Time horaLlegada, boolean estado) {
        this.codigo = codigo;
        this.numeroBus = numeroBus;
        this.descripcion = descripcion;
        this.horaSalida = horaSalida;
        this.horaLlegada = horaLlegada;
        this.estado = estado;
    }

    public int getCodigo() {
        return codigo;
    }

    public int getNumeroBus() {
        return numeroBus;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Time getHoraSalida() {
        return horaSalida;
    }

    public Time getHoraLlegada() {
        return horaLlegada;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setNumeroBus(int numeroBus) {
        this.numeroBus = numeroBus;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setHoraSalida(Time horaSalida) {
        this.horaSalida = horaSalida;
    }

    public void setHoraLlegada(Time horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Horario{" + "codigo=" + codigo + ", numeroBus=" + numeroBus + ", descripcion=" + descripcion + ", horaSalida=" + horaSalida + ", horaLlegada=" + horaLlegada + ", estado=" + estado + '}';
    }
    
    
    
}
