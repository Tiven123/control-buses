/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.sql.Time;

/**
 *
 * @author Steven
 */
public class Recorido {
    private int codigo;
    private int numeroBus;
    private String descripcion;
    private Time duracion;
    private String zona;
    private int cantidad;
    private String estado;

    public Recorido() {
    }

    public Recorido(int codigo, int numeroBus, String descripcion, Time duracion, String zona, int cantidad, String estado) {
        this.codigo = codigo;
        this.numeroBus = numeroBus;
        this.descripcion = descripcion;
        this.duracion = duracion;
        this.zona = zona;
        this.cantidad = cantidad;
        this.estado = estado;
    }

    public int getCodigo() {
        return codigo;
    }

    public int getNumeroBus() {
        return numeroBus;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Time getDuracion() {
        return duracion;
    }

    public String getZona() {
        return zona;
    }

    public int getCantidad() {
        return cantidad;
    }

    public String getEstado() {
        return estado;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setNumeroBus(int numeroBus) {
        this.numeroBus = numeroBus;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setDuracion(Time duracion) {
        this.duracion = duracion;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Recorido{" + "codigo=" + codigo + ", numeroBus=" + numeroBus + ", descripcion=" + descripcion + ", duracion=" + duracion + ", zona=" + zona + ", cantidad=" + cantidad + ", estado=" + estado + '}';
    }
    
    
    
    
}
