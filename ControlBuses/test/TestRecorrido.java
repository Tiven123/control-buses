/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Entidades.Recorrido;
import LogicaNegocios.RecorridoLogica;
import java.sql.Time;
import java.util.ArrayList;

/**
 *
 * @author Steven
 */
public class TestRecorrido {

    public TestRecorrido() {
    }
    RecorridoLogica recorridoLN;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        recorridoLN = new RecorridoLogica();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void insertarRecorridoCorrectamente() {
        System.out.println("Acontinuacion se hara la prueba para insertar correctamente");
        System.out.println("Se crea instancia y se llena con datos");
        Recorrido recorridoNuevo = new Recorrido();
        recorridoNuevo.setNumeroBus(344);
        recorridoNuevo.setDescripcion("Chiles a Quesada");
        recorridoNuevo.setDuracion(Time.valueOf("1:10:10"));
        recorridoNuevo.setZona("Florencia-Muelle");
        recorridoNuevo.setCantidad(15);
        recorridoNuevo.setEstado(true);
        System.out.println("Se envia al metodo encargado de realizar la insercion a base de datos el objeto cargado de datos");
        System.out.println("se almacena respuesta");
        boolean insertar = recorridoLN.insertar(recorridoNuevo);
        System.out.println("se valida respuesta");
        assertTrue(insertar);
        System.out.println("insertado correctamente");
        fail("error al insertar");
    }

    @Test
    public void insertarRecorridoConDatosIncompletos() {
        System.out.println("Acontinuacion se hara la prueba para insertar incorrectamente");
        System.out.println("Se crea instancia del objeto y se llena con datos sin descripcion ni numero de bus");
        Recorrido recorridoNuevo = new Recorrido();
        recorridoNuevo.setDuracion(Time.valueOf("1:10:10"));
        recorridoNuevo.setZona("Florencia-Muelle");
        recorridoNuevo.setCantidad(15);
        recorridoNuevo.setEstado(true);
        System.out.println("Se envia al metodo encargado de realizar la insercion a base de datos el objeto cargado de datos");
        System.out.println("se almacena respuesta");
        boolean insertar = recorridoLN.insertar(recorridoNuevo);
        System.out.println("se valida respuesta");
        assertFalse(insertar);
        System.out.println("no insertado");
        fail("error al insertar objeto con datos incompletos");

    }

    @Test
    public void buscarRecorrido() {
        System.out.println("Acontinuacion se hara la prueba para buscar ");
        System.out.println("Se asigna un codigo existente");
        int codigo = 1;
        System.out.println("Se envia al metodo encargado de realizar la busqueda a base de datos con el codigo a buscar");
        System.out.println("se almacena respuesta");
        Recorrido busqueda = recorridoLN.buscar(codigo);
        System.out.println("se valida respuesta");
        Recorrido recorridoNuevo = new Recorrido();
        recorridoNuevo.setCodigo(1);
        recorridoNuevo.setNumeroBus(344);
        recorridoNuevo.setDescripcion("Chiles a Quesada");
        recorridoNuevo.setDuracion(Time.valueOf("1:10:10"));
        recorridoNuevo.setZona("Florencia-Muelle");
        recorridoNuevo.setCantidad(15);
        recorridoNuevo.setEstado(true);;
        assertEquals(busqueda, recorridoNuevo);
        System.out.println("encontrado");
        fail("error al buscar un objeto existente");

    }

    @Test
    public void buscarRecorridoInexistente() {
        System.out.println("Acontinuacion se hara la prueba para buscar un objeto inexistente");
        System.out.println("Se asigna un codigo inexistente");
        int codigo = 9867;
        System.out.println("Se envia al metodo encargado de realizar la busqueda a base de datos con el codigo a buscar");
        System.out.println("se almacena respuesta");
        Recorrido busqueda = recorridoLN.buscar(codigo);
        System.out.println("se valida respuesta");
        Recorrido horarioNuevo = new Recorrido();
        assertEquals(busqueda, horarioNuevo);
        System.out.println("no encontrado");
        fail("error al buscar un objeto inexistente");

    }

    @Test
    public void modificarRecorrido() {
        System.out.println("Acontinuacion se hara la prueba para modificar");
        System.out.println("Se asigna un codigo existente");
        int codigo = 1;
        System.out.println("Se envia al metodo encargado de realizar la modificacion a base de datos con el codigo a modificar");
        Recorrido recorridoNuevo = new Recorrido();
        recorridoNuevo.setNumeroBus(389);
        recorridoNuevo.setDescripcion("Chiles");
        recorridoNuevo.setDuracion(Time.valueOf("1:10:10"));
        recorridoNuevo.setZona("Florencia");
        recorridoNuevo.setCantidad(45);
        recorridoNuevo.setEstado(true);
        System.out.println("se almacena respuesta");
        Recorrido busqueda = recorridoLN.modificar(codigo, recorridoNuevo);
        System.out.println("se valida respuesta");
        assertEquals(busqueda, recorridoNuevo);
        System.out.println("objeto modificado");
        fail("error al modificar un objeto existente");

    }

    @Test
    public void modificarRecorridoCambioCodigo() {
        System.out.println("Acontinuacion se hara la prueba para modificar un recorrido");
        System.out.println("Se asigna un codigo existente");
        int codigo = 1;
        System.out.println("Se envia al metodo encargado de realizar la modificacion a base de datos con el codigo a modificar");
        Recorrido recorridoNuevo = new Recorrido();
        recorridoNuevo.setNumeroBus(389);
        recorridoNuevo.setDescripcion("Chiles");
        recorridoNuevo.setDuracion(Time.valueOf("1:10:10"));
        recorridoNuevo.setZona("Florencia");
        recorridoNuevo.setCantidad(45);
        recorridoNuevo.setEstado(true);
        System.out.println("se almacena respuesta");
        Recorrido busqueda = recorridoLN.modificar(codigo, recorridoNuevo);
        System.out.println("se valida respuesta");
        recorridoNuevo = new Recorrido();
        assertEquals(busqueda, recorridoNuevo);
        System.out.println(" no modificado");
        fail("error al modificar el codigo existente");

    }

    @Test
    public void eliminarRecorrido() {
        System.out.println("Acontinuacion se hara la prueba para eliminar un recorrido");
        System.out.println("Se asigna un codigo existente");
        int codigo = 1;
        System.out.println("Se envia al metodo encargado de realizar la eliminacion logica con el codigo");
        System.out.println("se almacena respuesta");
        boolean respuesta = recorridoLN.eliminar(codigo);
        System.out.println("se valida respuesta");
        assertTrue(respuesta);
        System.out.println("eliminado");
        fail("error al eliminar existente");

    }

    @Test
    public void eliminarRecorridoInexistente() {
        System.out.println("Acontinuacion se hara la prueba para eliminar un recorrido inexistente");
        System.out.println("Se asigna un codigo inexistente");
        int codigo = 158;
        System.out.println("Se envia al metodo encargado de realizar la eliminacion logica con el codigo");
        System.out.println("se almacena respuesta");
        boolean respuesta = recorridoLN.eliminar(codigo);
        System.out.println("se valida respuesta");
        assertFalse(respuesta);
        System.out.println("no eliminado");
        fail("error al eliminar inexistente");

    }

    @Test
    public void reporteRecorridoUnidad() {
        System.out.println("Acontinuacion se hara la prueba para generar un reporte recorrido");
        System.out.println("Se ejecuta el metodo encargado de realizar el reporte");
        System.out.println("se almacena respuesta");
        ArrayList<Recorrido> respuesta = recorridoLN.reporte();
        System.out.println("se valida respuesta");
        assertFalse(respuesta.isEmpty());
        System.out.println("reporte Generado");
        fail("error al generar reporte");

    }

    @Test
    public void reporteRecorridoBaseSinDatos() {
        System.out.println("Acontinuacion se hara la prueba para generar un reporte recorrido");
        System.out.println("Se ejecuta el metodo encargado de realizar el reporte");
        System.out.println("se almacena respuesta");
        ArrayList<Recorrido> respuesta = recorridoLN.reporte();
        System.out.println("se valida respuesta");
        Recorrido recorridoNuevo = new Recorrido();
        assertEquals(recorridoNuevo, respuesta.get(0));
        System.out.println("reporte  no Generado");
        fail("error al generar reporte sin datos");

    }
}
