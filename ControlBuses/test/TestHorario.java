/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Entidades.Horario;
import LogicaNegocios.HorarioLogica;
import java.sql.Time;
import java.util.ArrayList;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Steven
 */
public class TestHorario {

    public TestHorario() {
    }
    HorarioLogica horarioLN;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        System.out.println("--------------------------------------Test-------------------------------");
        horarioLN = new HorarioLogica();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void insertarHorarioCorrectamente() {
        System.out.println("Acontinuacion se hara la prueba para insertar correctamente un horario");
        System.out.println("Se crea instancia del horario y se llena con datos");
        Horario horarioNuevo = new Horario();
        horarioNuevo.setNumeroBus(344);
        horarioNuevo.setDescripcion("Chiles a Quesada");
        horarioNuevo.setHoraSalida(Time.valueOf("11:10:10"));
        horarioNuevo.setHoraLlegada(Time.valueOf("12:30:45"));
        horarioNuevo.setEstado(true);
        System.out.println("Se envia al metodo encargado de realizar la insercion a base de datos el objeto horario cargado de datos");
        System.out.println("se almacena respuesta");
        boolean insertar = horarioLN.insertar(horarioNuevo);
        System.out.println("se valida respuesta");
        assertTrue(insertar);
        System.out.println("horario insertado correctamente");
        //fail("error al insertar horario");
    }

    @Test
    public void insertarHorarioConDatosIncompletos() {
        System.out.println("Acontinuacion se hara la prueba para insertar incorrectamente un horario");
        System.out.println("Se crea instancia del horario y se llena con datos sin descripcion ni numero de bus");
        Horario horarioNuevo = new Horario();
        horarioNuevo.setHoraSalida(Time.valueOf("11:10:10"));
        horarioNuevo.setHoraLlegada(Time.valueOf("12:30:45"));
        horarioNuevo.setEstado(true);
        System.out.println("Se envia al metodo encargado de realizar la insercion a base de datos el objeto horario cargado de datos");
        System.out.println("se almacena respuesta");
        boolean insertar = horarioLN.insertar(horarioNuevo);
        System.out.println("se valida respuesta");
        assertFalse(insertar);
        System.out.println("horario no insertado");
    }

    @Test
    public void buscarHorario() {
        System.out.println("Acontinuacion se hara la prueba para buscar un horario");
        System.out.println("Se asigna un codigo de horario existente");
        int codigo = 3;
        System.out.println("Se envia al metodo encargado de realizar la busqueda a base de datos con el codigo a buscar");
        System.out.println("se almacena respuesta");
        Horario busqueda = horarioLN.buscar(codigo);
        System.out.println("se valida respuesta" + busqueda);
        boolean respuesta;
        if (busqueda.getNumeroBus() == 344 & busqueda.getDescripcion().equals("Chiles a Quesada") & busqueda.getHoraSalida().equals(Time.valueOf("11:10:10"))
                & busqueda.getHoraLlegada().equals(Time.valueOf("12:30:45")) & busqueda.getEstado() && true) {
            respuesta = true;
        } else {
            respuesta = false;
        }
        System.out.println("valor" + respuesta);
        assertTrue(respuesta);
        System.out.println("horario encontrado");

    }

    @Test
    public void buscarHorarioInexistente() {
        System.out.println("Acontinuacion se hara la prueba para buscar un horario inexistente");
        System.out.println("Se asigna un codigo de horario inexistente");
        int codigo = 189;
        System.out.println("Se envia al metodo encargado de realizar la busqueda a base de datos con el codigo a buscar");
        System.out.println("se almacena respuesta");
        Horario busqueda = horarioLN.buscar(codigo);
        System.out.println("se valida respuesta" + busqueda);
        boolean respuesta;
        if (busqueda.getNumeroBus() == 0 & busqueda.getDescripcion() == null & busqueda.getHoraSalida() == null
                & busqueda.getHoraLlegada() == null & busqueda.getEstado() == false) {
            respuesta = true;
        } else {
            respuesta = false;
        }
        assertTrue(respuesta);
        System.out.println("horario no encontrado");
    }

    @Test
    public void modificarHorario() {
        System.out.println("Acontinuacion se hara la prueba para modificar un horario");
        System.out.println("Se asigna un codigo de horario existente");
        int codigo = 1;
        System.out.println("Se envia al metodo encargado de realizar la modificacion a base de datos con el codigo a modificar");
        Horario horarioNuevo = new Horario();
        horarioNuevo.setNumeroBus(3484);
        horarioNuevo.setDescripcion("Muelle a Platanar");
        horarioNuevo.setHoraSalida(Time.valueOf("11:45:10"));
        horarioNuevo.setHoraLlegada(Time.valueOf("1:10:45"));
        System.out.println("se almacena respuesta");
        Horario busqueda = horarioLN.modificar(codigo, horarioNuevo);
        System.out.println("se valida respuesta " + busqueda);
        boolean respuesta;
        if (busqueda.getDescripcion().equals(horarioNuevo.getDescripcion())) {
            respuesta = true;
        } else {
            respuesta = false;
        }
        assertTrue(respuesta);
        System.out.println("horario modificado");

    }

    @Test
    public void modificarHorarioInexistente() {
        System.out.println("Acontinuacion se hara la prueba para modificar un horario");
        System.out.println("Se asigna un codigo de horario inexistente");
        int codigo = 155;
        System.out.println("Se envia al metodo encargado de realizar la modificacion a base de datos con el codigo a modificar");
        Horario horarioNuevo = new Horario();
        System.out.println("se almacena respuesta");
        Horario busqueda = horarioLN.modificar(codigo, horarioNuevo);
        System.out.println("se valida respuesta");
        boolean resultado;
        if (busqueda.getDescripcion() == null) {
            resultado = false;
        } else {
            resultado = true;
        }
        assertFalse(resultado);
        System.out.println("horario no modificado");

    }

    @Test
    public void eliminarHorario() {
        System.out.println("Acontinuacion se hara la prueba para eliminar un horario");
        System.out.println("Se asigna un codigo de horario existente");
        int codigo = 1;
        System.out.println("Se envia al metodo encargado de realizar la eliminacion logica con el codigo");
        System.out.println("se almacena respuesta");
        boolean respuesta = horarioLN.eliminar(codigo);
        System.out.println("se valida respuesta");
        assertTrue(respuesta);
        System.out.println("horario eliminado");

    }

    @Test
    public void eliminarHorarioInexistente() {
        System.out.println("Acontinuacion se hara la prueba para eliminar un horario inexistente");
        System.out.println("Se asigna un codigo de horario inexistente");
        int codigo = 158;
        System.out.println("Se envia al metodo encargado de realizar la eliminacion logica con el codigo");
        System.out.println("se almacena respuesta");
        boolean respuesta = horarioLN.eliminar(codigo);
        System.out.println("se valida respuesta");
        assertFalse(respuesta);
        System.out.println("horario  no eliminado");

    }

    @Test
    public void reporteHorarioUnidad() {
        System.out.println("Acontinuacion se hara la prueba para generar un reporte horarios");
        System.out.println("Se ejecuta el metodo encargado de realizar el reporte de horarios");
        System.out.println("se almacena respuesta");
        LinkedList<Horario> respuesta = horarioLN.reporte();
        System.out.println("se valida respuesta" + respuesta);
        assertFalse(respuesta.isEmpty());
        System.out.println("reporte Generado");

    }

    @Test
    public void reporteHorarioVerificaDatos() {
        System.out.println("Acontinuacion se hara la prueba para generar un reporte horarios");
        System.out.println("Se ejecuta el metodo encargado de realizar el reporte de horarios");
        System.out.println("se almacena respuesta");
        LinkedList<Horario> respuesta = horarioLN.reporte();
        System.out.println("se valida respuesta");
        boolean resultado;
        if (respuesta.get(0).getDescripcion() == null) {
            resultado = true;
        } else {
            resultado = false;
        }
        assertFalse(resultado);

        System.out.println("reporte  no Generado");

    }

}
