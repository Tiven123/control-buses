/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Entidades.Socio;
import LogicaNegocios.SocioLogica;

/**
 *
 * @author Steven
 */
public class TestSocio {

    public TestSocio() {
    }
    SocioLogica socioLN;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        socioLN = new SocioLogica();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void insertarSocioCorrectamente() {
        System.out.println("Acontinuacion se hara la prueba para insertar correctamente");
        System.out.println("Se crea instancia y se llena con datos");
        Socio socioNuevo = new Socio();
        socioNuevo.setNombre("Carlos Andres");
        socioNuevo.setApellidos("Vargas Castro");
        socioNuevo.setLinea("Transpisa");
        socioNuevo.setNumeroBus(3589);
        socioNuevo.setCapacidad(85);
        socioNuevo.setChofer("Juan Jimenez");
        socioNuevo.setAyudante("Fredry Campos");
        socioNuevo.setEstado(true);
        System.out.println("Se envia al metodo encargado de realizar la insercion a base de datos el objeto cargado de datos");
        System.out.println("se almacena respuesta");
        boolean insertar = socioLN.insertar(socioNuevo);
        System.out.println("se valida respuesta");
        assertTrue(insertar);
        System.out.println("insertado correctamente");
       
    }

    @Test
    public void insertarSocioConDatosIncompletos() {
        System.out.println("Acontinuacion se hara la prueba para insertar incorrectamente");
        System.out.println("Se crea instancia del objeto y se llena con datos sin capacidad ni numero de bus");
        Socio socioNuevo = new Socio();
        socioNuevo.setNombre("Carlos Andres");
        socioNuevo.setApellidos("Vargas Castro");
        socioNuevo.setLinea("Transpisa");
        socioNuevo.setChofer("Juan Jimenez");
        socioNuevo.setAyudante("Fredry Campos");
        socioNuevo.setEstado(true);
        System.out.println("Se envia al metodo encargado de realizar la insercion a base de datos el objeto cargado de datos");
        System.out.println("se almacena respuesta");
        boolean insertar = socioLN.insertar(socioNuevo);
        System.out.println("se valida respuesta");
        assertFalse(insertar);
        System.out.println("no insertado");
        
    }
}
